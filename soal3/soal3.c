#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<syslog.h>
#include<string.h>
#include<stdbool.h>
#include<time.h>
#include<ctype.h>
#include<limits.h>
#include<dirent.h>
#include<math.h>
#include<wait.h>

void making(char bash[], char *arg[]){
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execv(bash, arg);
    }
    else{
        ((wait(&status))>0);
    }
}

void moving(char bash[], char file[], char target[]) {
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execlp(bash, bash, file, target, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}

void removing(char bash[], char file[]) {
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execlp(bash, bash, file, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}


int main(int argc, char const *argv[])
{
  	int status;
  	pid_t child_id;
  	child_id = fork();
  	struct dirent *ep;
  	
  	const char* direct = "/home/nafts/modul2/";
    	char animal[] = "/home/nafts/modul2/animal";
    	char *habitat[] = {"/home/nafts/modul2/darat", "/home/nafts/modul2/air"};
    	char *user;
    	user=(char *)malloc(10*sizeof(char));
    	user=getlogin();

    	char *folder[] = {"mkdir", "/home/ubuntu/modul2", NULL};
    	char *fdarat[] = {"mkdir", "/home/ubuntu/modul2/darat", NULL};
    	char *fair[] = {"mkdir", "/home/ubuntu/modul2/air", NULL};
    	char *unzipf[] =  {"unzip", "animal.zip", "-d", "/home/ubuntu/modul2", NULL};

    	making("/bin/mkdir", folder);
    	making("/bin/mkdir", fdarat);
    	sleep(3);
    	making("/bin/mkdir", fair);

    	making("/usr/bin/unzip", unzipf);

DIR *dp;
dp = opendir(animal);
    		
if(dp != NULL) {
   while((ep = readdir(dp))) {
   	if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "darat")) {
        char path[100] = "/home/nafts/modul2/animal/";
        strcat(path, ep->d_name); 
        moving("mv", path, "/home/nafts/modul2/darat");
        } else if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "air")) {
                char path[100] = "/home/nafts/modul2/animal/";
                strcat(path, ep->d_name); 
                moving("mv", path, "/home/nafts/modul2/air");
            	 } else {
                if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
                char path[100] = "/home/nafts/modul2/animal/";
                strcat(path, ep->d_name); 
                removing("rm", path);
                					}
            					}
        			}
        		(void) closedir (dp);
    		} else  perror ("Couldn't open the directory");
    DIR *darat;

    darat = opendir(habitat[0]);
    if(darat != NULL) {
        while((ep = readdir(darat))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "bird")) {
                char path[100] = "/home/nafts/modul2/darat/";
                strcat(path, ep->d_name); 
                removing("rm", path);
            }
        }
        (void) closedir (darat);
    } else  perror ("Couldn't open the directory");
    DIR *air;

    air = opendir(habitat[1]);
    FILE *air_list = fopen("/home/nafts/modul2/air/list.txt", "w");

    struct stat fs;
    int r;

    if(air != NULL) {
         while((ep = readdir(air))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "jpg")) {
                char path[100] = "/home/nafts/modul2/air/";
                strcat(path, ep->d_name);
                r = stat(path, &fs);
                char pread = '_', pwrite = '_', pexec= '_';
                if( fs.st_mode & S_IRUSR ) pread = 'r';
                if( fs.st_mode & S_IWUSR ) pwrite = 'w';
                if( fs.st_mode & S_IXUSR ) pexec = 'x';
                fprintf(air_list, "%s_%c%c%c_%s\n", user, pread, pwrite, pexec, ep->d_name);
            } else if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "png")) {
                char path[100] = "/home/nafts/modul2/air/";
                strcat(path, ep->d_name);
                r = stat(path, &fs);
                char pread = '_', pwrite = '_', pexec= '_';
                if( fs.st_mode & S_IRUSR ) pread = 'r';
                if( fs.st_mode & S_IWUSR ) pwrite = 'w';
                if( fs.st_mode & S_IXUSR ) pexec = 'x';
                fprintf(air_list, "%s_%c%c%c_%s\n", user, pread, pwrite, pexec, ep->d_name);
            }
        }
        (void) closedir (air);
    } else  perror ("Couldn't open the directory");

    fclose(air_list);

}
