#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stddef.h>
#include <dirent.h>

void download1()
{
  printf("Ini untuk download weapons");
  char downloadweapon[1000] = 
  {
    "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
  };
  char weaponzip[1000] = 
  {
    "weapons.zip"
  };

    pid_t child_id;
    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
      char *argv[] = {"wget", "--no-check-certificate", downloadweapon, "-O", weaponzip, NULL};
      execv("/bin/wget", argv);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
  
}

void download2()
{
  printf("Ini untuk download characters");
  char downloadcharacters[1000] = 
  {
    "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download"
  };
  char characterszip[1000] = 
  {
    "characters.zip"
  };

  
    pid_t child_id;
    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
      char *argv[] = {"wget", "--no-check-certificate", downloadcharacters, "-O", characterszip, NULL};
      execv("/bin/wget", argv);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
  
}

void makedirectory()
{
  DIR *gachafolder = opendir("gacha_gacha");
  pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
      char *argv_gachafolder[] = {"mkdir", "gacha_gacha", NULL};
      execv("/bin/mkdir", argv_gachafolder);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
}

void unzipper() // buat ngeunzip character sama weapons
{
  pid_t child_id;
  child_id = fork();

    char *folder[] = {"unzip", "*.zip"};

    execv("/bin/unzip",folder);
}

void zipper()
{
// $ zip -r <output_file> <folder_1> <folder_2> ... <folder_n> (ini cara ngezip)
printf("Start zipping files");
char zipfolder[1000] = {
  "gacha_gacha"
};
char zipoutput[1000] = {
  "not_safe_for_wibu.zip"
};
 pid_t child_id;
    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
      char *argv[] = {"zip", "-p" "satuduatiga" "-q", "-r", zipoutput, zipfolder, NULL};
      execv("/bin/zip", argv);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
}

int main()
{
 download1(); //download character buat soal 1a
 download2(); //download weapons buat soal 1a
 makedirectory(); //bikin directory buat soal 1a
 unzipper(); //unzip file yang udah didownload buat soal 1a
 zipper(); //ngezip soal buat soal 1e dari folder gacha_gacha biar jadi zip not_safe_for_wibu.zip
 password();
}

// Template Program
// int main() {
//   pid_t pid, sid;        // Variabel untuk menyimpan PID

//   pid = fork();     // Menyimpan PID dari Child Process

//   /* Keluar saat fork gagal
//   * (nilai variabel pid < 0) */
//   if (pid < 0) {
//     exit(EXIT_FAILURE);
//   }

//   /* Keluar saat fork berhasil
//   * (nilai variabel pid adalah PID dari child process) */
//   if (pid > 0) {
//     exit(EXIT_SUCCESS);
//   }

//   umask(0);

//   sid = setsid();
//   if (sid < 0) {
//     exit(EXIT_FAILURE);
//   }

//   if ((chdir("/")) < 0) {
//     exit(EXIT_FAILURE);
//   }

//   close(STDIN_FILENO);
//   close(STDOUT_FILENO);
//   close(STDERR_FILENO);

//   while (1) {
//     download1();
//     download2();

//     sleep(30);
//   }
// }
