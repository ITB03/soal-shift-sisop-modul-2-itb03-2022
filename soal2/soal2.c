#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

char folder[] = "/home/dimas/shift2/drakor/";

void makedir(char *direktori){
    pid_t child_id = fork();
    int status;

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        execl("/bin/mkdir", "mkdir", "-p", direktori, NULL);
    }else{
        ((wait(&status)) > 0);
    }
}

void unzipfile(){
    pid_t child_id = fork();
    int status;

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        execl("/bin/unzip", "unzip", "drakor.zip", "-d", "/home/dimas/shift2/drakor", "*.png", NULL);
    }else{
        (wait(&status)) > 0;
    }
}

int main(){
    char path[] = "/home/dimas/shift2/drakor/";
    makedir(path);
    unzipfile();

}