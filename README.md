# soal-shift-sisop-modul-2-ITB03-2022

soal-shift-sisop-modul-2-ITB03-2022

# Readme Modul 2

### Kelompok ITB 03

| Nama Anggota | NRP |
| --- | --- |
| Naftali Salsabila Kanaya Putri | 5027201012 |
| Dimas Bagus Rachmadani | 5027201034 |
| Ariel Daffansyah Aliski | 5027201058 |

# Soal Nomor 1

### Pengerjaan

| Ariel Daffansyah Aliski | 5027201058 |
| --- | --- |

Mas Refadi adalah seorang wibu gemink. Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu.

a. `Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada **dibawah**, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. **Penjelasan sistem gacha ada di poin (d).*`

Jawab :

Pada soal ini, saya membuat sebuah fungsi untuk mendownload file characters dan file weapons sebagai berikut

```c
void download1()
{
  printf("Ini untuk download weapons");
  char downloadweapon[1000] = 
  {
    "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"
  };
  char weaponzip[1000] = 
  {
    "weapons.zip"
  };

    pid_t child_id;
    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
      char *argv[] = {"wget", "--no-check-certificate", downloadweapon, "-O", weaponzip, NULL};
      execv("/bin/wget", argv);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
  
}
```

Fungsi download dilakukan dengan menjadikan sebuah child process yang dilakukan dengan cara link download dibuat menjadi sebuah array yang elemennya diakses dalam array argv dibawah, lalu dari array argv tersebut diambil dalam perintah execv prinsip kerjanya mirip seperti mengirim sebuah perintah kepada terminal, bedanya ini dilakukan dengan mengakses file asli dari fungsi tersebut

Dengan prinsip kerja yang sama seperti download, saya membuat fungsi untuk membuat direktori gacha_gacha

```c
void makedirectory()
{
  DIR *gachafolder = opendir("gacha_gacha");
  pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
      char *argv_gachafolder[] = {"mkdir", "gacha_gacha", NULL};
      execv("/bin/mkdir", argv_gachafolder);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
}
```

karena fungsi ingin untuk mengextract hasil dari gacha, maka dibuatlah fungsi unzipper dengan prinsip yang sama

```c
void makedirectory()
{
  DIR *gachafolder = opendir("gacha_gacha");
  pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
      char *argv_gachafolder[] = {"mkdir", "gacha_gacha", NULL};
      execv("/bin/mkdir", argv_gachafolder);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
}
```

**Contoh : 157_characters_5_Albedo_53880**

e. `Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada **30 Maret jam 04:44**. Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka **3 jam** setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)`

Jawab :

Untuk mengzip program, dibuat sebuah fungsi yang prinsip kerjanya sama seperti pada semua fungsi 1a

```c
void zipper()
{
// $ zip -r <output_file> <folder_1> <folder_2> ... <folder_n> (ini cara ngezip)
printf("Start zipping files");
char zipfolder[1000] = {
  "gacha_gacha"
};
char zipoutput[1000] = {
  "not_safe_for_wibu.zip"
};
 pid_t child_id;
    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);
    if(child_id == 0)
    {
      char *argv[] = {"zip", "-q", "-r", zipoutput, zipfolder, NULL};
      execv("/bin/zip", argv);
      exit(EXIT_SUCCESS);
    } 
    else
    {
      wait(NULL);
    }
}
```

Akan tetapi, program membutuhkan agar zip tersebut dipassword, sehingga saya merubah char untuk yang diakses execv sebagai berikut

```c
char *argv[] = {"zip", "-p" "satuduatiga" "-q", "-r", zipoutput, zipfolder, NULL};
```

Hasil folder yang dikerjakan adalah sebgai berikut

Note :

- file unzip hanya saya buat untuk mengetes fungsionalitas mengunzip karena harus memberi delay waktu yang tepat pada fungsi
- File bawah (invalid encoding) terbuat ketika mendownload zip

![Untitled](img/Untitled.png)

**Note**:

- Menggunakan fork dan exec.
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename().
- Tidak boleh pake cron.
- Semua poin dijalankan oleh **1** **script di latar belakang.** Cukup jalankan script 1x serta ubah time dan date untuk check hasilnya.
- Link

Database item characters :

[https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view](https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view)

Database item weapons :

[https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view](https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view)

**Tips :**

- Gacha adalah proses untuk mendapatkan suatu item dengan cara melakukan randomize dari seluruh item yang ada.
- DIkarenakan file database memiliki format (.json). Silahkan gunakan library **<json-c/json.h>**, install dengan “**apt install libjson-c-dev”**, dan compile dengan “**gcc [nama_file] -l json-c -o [nama_file_output]”**. Silahkan gunakan dokumentasi berikut untuk membaca dan parsing file (.json).

[https://progur.com/2018/12/how-to-parse-json-in-c.html](https://progur.com/2018/12/how-to-parse-json-in-c.html)

- Silahkan gunakan **Dynamic Memory Allocation** pada C untuk menyimpan isi file (.json) jika file tersebut terlalu besar untuk disimpan saat parsing.

# Soal Nomor 2

### Pengerjaan

| Dimas Bagus Rachmadani | 5027201034 |
| --- | --- |

Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya

a. `Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.`

Jawab:

Pada sub soal ini saya menggunakan fungsi makedir() dan unzipfile() untuk membuat direktori serta melakukan unzip drakor.zip pada /home/[user]/shift2/drakor sebagai berikut:

```c
void makedir(char *direktori){
    pid_t child_id = fork();
    int status;

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        execl("/bin/mkdir", "mkdir", "-p", direktori, NULL);
    }else{
        ((wait(&status)) > 0);
    }
}
```

```c
void unzipfile(){
    pid_t child_id = fork();
    int status;

    if (child_id < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        execl("/bin/unzip", "unzip", "drakor.zip", "-d", "/home/dimas/shift2/drakor", "*.png", NULL);
    }else{
        (wait(&status)) > 0;
    }
}
```

Kedua fungsi tersebut menggunakan forking untuk membuat 2 proses yaitu child dan parent. Untuk fungsi makedir(), child proses akan menjalankan program mkdir pada linux dengan command mkdir -p menuju direktori yang diinginkan yaitu /home/[user]/shift2/drakor. Lalu parent proses diprogram untuk wait/menunggu child proses selesai mengeksekusi program. Kemudian fungsi unzipfile() akan dijalankan setelah child proses fungsi makedir() selesai membuat direktori. Untuk fungsi unzipfile() sendiri mirip seperti fungsi makedir() yaitu menggunakan fork untuk membuat proses kemudian child proses akan menjalankan program unzip file pada linux dengan command unzip -d menuju direktori yang diinginkan yaitu /home/[user]/shift2/drakor. Kemudian terdapat tambahan command yaitu “*.png” untuk mengunzip file khusus untuk file berformat “.png”. 

Hasil dari unzip drakor.zip ditampilkan sebagai berikut: 

![soal2a.png](img/soal2a.png)

### Note:

- File zip berada dalam drive modul shift ini bernama drakor.zip
- File yang penting hanyalah berbentuk .png
- Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan *underscore*(_).
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di bahasa C.
- Gunakan bahasa pemrograman C (**Tidak boleh yang lain)**.
- Folder shift2, drakor, dan kategori dibuatkan oleh program **(Tidak Manual)**.
- [user] menyesuaikan nama user linux di os anda.

### Kendala:

Kendala pada pengerjaan soal ini adalah kurangnya penguasaan pada program menggunakan manipulasi string sehingga pengerjaan pada sub nomer setelahnya belum bisa diselesaikan.

# Soal Nomor 3

### Pengerjaan

| Naftali Salsabila Kanaya Putri | 5027201012 |
| --- | --- |

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

### Note:

- Tidak boleh memakai system().
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec dan fork
- Direktori “.” dan “..” tidak termasuk


a. `Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.`

Jawab:

Pada soal ini saya membuat fungsi untuk membuat directory terlebih dahulu dimana dalam fungsi ini nanti akan menerima passing parameter berupa command bash dan argumennya.

```c
void making(char bash[], char *arg[]){
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execv(bash, arg);
    }
    else{
        ((wait(&status))>0);
    }
}
```
Setelah itu pada `int main` akan mendefinisikan char `folder` yaitu command untuk membuat dan menentukan path folder yang akan menampung folder air, darat, dan animal, `fdarat` yaitu command untuk membuat dan menentukan path folder untuk menampung file-file binatang darat,`fair` yaitu command untuk membuat dan menentukan path folder untuk menampung file-file binatang air. Setelah itu mempassing command bash dan argumennya ke function `making ` dimana yang dipassing adalah command `mkdir`. Dalam `sleep(3)` setelah membuat directory darat maka akan delay 3 detik yang kemudian berlanjut untuk membuat directory air.

```c
char *folder[] = {"mkdir", "/home/ubuntu/modul2", NULL};
char *fdarat[] = {"mkdir", "/home/ubuntu/modul2/darat", NULL};
char *fair[] = {"mkdir", "/home/ubuntu/modul2/air", NULL};

making("/bin/mkdir", folder);
making("/bin/mkdir", fdarat);
sleep(3);
making("/bin/mkdir", fair);
```

b. `Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.`
Jawab:

Pada sub soal ini masih menggunakan function `making` yang akan membash commandnya dimana pada `int main` didefinisikan dulu char `unzipf` untuk mengunzip folder yang dimaksud. Kemudian dilakukan pemanggilan function `making` dan yang dipassing adalah command `unzip` dan argumennya dari char `unzipf`

```c
char *unzipf[] =  {"unzip", "animal.zip", "-d", "/home/ubuntu/modul2", NULL};
making("/usr/bin/unzip", unzipf);
```

**Output**

Hasil directory yang terbuat pada `soal 3a` dan folder dari unzip `animal.zip` :

![folder-folder yang terbuat](img/foldersmade.jpeg)

Dengan file-file yang terunzip pada `soal 3b` sebelum dipindahkan ke masing-masing folder berdasarkan jenis hewan :

![Unzip](img/unzipanimal.jpeg)

c. `Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.`

Jawab:

Pada sub soal ini saya membuat function `moving` untuk memindahkan file dari folder asal ke folder destination.

```c
void moving(char bash[], char file[], char target[]) {
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execlp(bash, bash, file, target, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}
```

Dan juga function `removing` untuk menghapus file.

```c
void removing(char bash[], char file[]) {
    int status;
    pid_t child;
    child = fork();
    if(child == 0){
        execlp(bash, bash, file, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}
```

Pada `int main` membuka directory `animal` yang file-file di dalamnya akan disorting dan dimasukkan ke folder yang sesuai

```c
DIR *dp;
dp = opendir(animal);
```

Setelah itu dilakukan looping sampai directorynya kosong agar semua file-file didalamnya benar-benar dipindahkan ke folder yang seharusnya. Dimana dalam `note` direktori “.” dan “..” tidak termasuk sehingga perlu disorting menggunakan `strcmp`. Penentuan file berdasarkan nama filenya dimana apakah `d_name` memiliki substring `darat` atau `air`, penyortingan substring dilakukan menggunakan `strstr`, dan apabila terdapat substring tersebut akan dipindahkan ke folder darat atau air dengan memanggil function `moving` dengan bash commandnya yaitu `mv`.Dan file-file yang tidak ada didalam kondisi memiliki substring `darat` maupun `air` akan dihapus dengan memanggil function `removing` dengan mempassing bash command `rm` .

```c
if(dp != NULL) {
   while((ep = readdir(dp))) {
   	if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "darat")) {
        char path[100] = "/home/nafts/modul2/animal/";
        strcat(path, ep->d_name); 
        moving("mv", path, "/home/nafts/modul2/darat");
        } else if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "air")) {
                char path[100] = "/home/nafts/modul2/animal/";
                strcat(path, ep->d_name); 
                moving("mv", path, "/home/nafts/modul2/air");
            	 } else {
                if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
                char path[100] = "/home/nafts/modul2/animal/";
                strcat(path, ep->d_name); 
                removing("rm", path);
                					}
            					}
        			}
        		(void) closedir (dp);
    		} else  perror ("Couldn't open the directory");
```

d. `Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.`

Jawab:

Seperti pada sub soal sebelumnya maka harus mengakses directory darat kemudian dilakukan looping sampai directorynya kosong dan file-file didalamnya akan disorting berdasarkan kondisi nama filenya. Apabila kondisinya bukan direktori “.” dan “..” dengan penyecekan menggunakan `strcmp` dan nama filenya memiliki substring `bird` dengan pengecekan menggunakan `strstr` maka akan dipanggil function `removing` dengan mempassing command bash `rm` dan `path` dari filenya.

```c
DIR *darat;
darat = opendir(habitat[0]);
if(darat != NULL) {
    while((ep = readdir(darat))) {
        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "bird")) {
          char path[100] = "/home/nafts/modul2/darat/";
          strcat(path, ep->d_name); 
          removing("rm", path);
            }
        }
        (void) closedir (darat);
    } else  perror ("Couldn't open the directory");
```

**Output**

Hasil pemindahan file-file ke folder yang sesuai pada `soal 3c` :

Folder hewan air :

![folder hewan air](img/airfolder.jpeg)

Folder hewan darat dan setelah penghapusan file `bird` pada `soal 3d` :

![folder hewan darat](img/daratfolder.jpeg)

e. `Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut. Contoh : conan_rwx_hewan.png`

Jawab:

Pada sub soal ini dilakukan akses pada directory `air` kemudian dilakukan looping untuk sampai direcorynya kosong. Looping dilakukan untuk menyortir nama file seperti pada sub soal sebelumnya dimana apabila kondisinya bukan direktori “.” dan “..” dengan penyecekan menggunakan `strcmp` dan nama filenya memiliki substring `jpg` atau `png` dengan pengecekan menggunakan `strstr`, jika iya maka akan dlist pada file `list.txt` dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut dan file permission adalah permission dari file tersebut.

```c
    DIR *air;
    air = opendir(habitat[1]);
    FILE *air_list = fopen("/home/nafts/modul2/air/list.txt", "w");

    struct stat fs;
    int r;

    if(air != NULL) {
         while((ep = readdir(air))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "jpg")) {
                char path[100] = "/home/nafts/modul2/air/";
                strcat(path, ep->d_name);
                r = stat(path, &fs);
                char pread = '_', pwrite = '_', pexec= '_';
                if( fs.st_mode & S_IRUSR ) pread = 'r';
                if( fs.st_mode & S_IWUSR ) pwrite = 'w';
                if( fs.st_mode & S_IXUSR ) pexec = 'x';
                fprintf(air_list, "%s_%c%c%c_%s\n", user, pread, pwrite, pexec, ep->d_name);
            } else if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "png")) {
                char path[100] = "/home/nafts/modul2/air/";
                strcat(path, ep->d_name);
                r = stat(path, &fs);
                char pread = '_', pwrite = '_', pexec= '_';
                if( fs.st_mode & S_IRUSR ) pread = 'r';
                if( fs.st_mode & S_IWUSR ) pwrite = 'w';
                if( fs.st_mode & S_IXUSR ) pexec = 'x';
                fprintf(air_list, "%s_%c%c%c_%s\n", user, pread, pwrite, pexec, ep->d_name);
            }
        }
        (void) closedir (air);
    } else  perror ("Couldn't open the directory");

    fclose(air_list);
```

**Output**

Isi dari `list.txt` yang berisi hewan-hewan air dengan format `UID` :

![list .txt](img/listtxt.jpeg)


